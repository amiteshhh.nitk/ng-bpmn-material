
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { ToastService } from '../../common/toast/toast.service';
import { BpmnService } from './../bpmn.service';

type taskCategory = {
    /**Id/pk representing task type e.g main*/
    type: string,
    /**Description for given task type. E.g Main*/
    name: string,
    tasks?: Array<any>,
    roles?: Array<string>,
    /**it will contain list of assignees keyed by role. e.g {'Safety-Role':[list of assigness]} */
    assignees?: { [role: string]: Array<any> },
    /**Reactive formGroup representing Add new row for a task type*/
    formGroup?: FormGroup
}
@Component({
    selector: 'app-bpmn-task',
    templateUrl: './bpmn-task.component.html',
    styleUrls: ['./bpmn-task.component.scss']
})
export class BpmnTaskComponent implements OnInit {
    tasks;
    saving = false;
    // selectedTabIndex = 0;//it controls save button visibility.
    taskCategories: Array<taskCategory> = [{ type: 'main', name: 'Main' }, { type: 'prerequisite', name: 'Pre-requisite' }, { type: 'review', name: 'Review' }];

    constructor(private toastService: ToastService, private bpmnService: BpmnService, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<any>) { }

    ngOnInit() {
        var tasks = this.cloneObj(this.data.selectedBpmn.tasks);
        this.setupTaskCategory(tasks);
    }

    private cloneObj(obj) {
        return JSON.parse(JSON.stringify(obj));//make a copy of obj
    }

    private getAllTasksFromTaskCategory() {
        var tasks = [];
        this.taskCategories.forEach(taskCategory => Array.prototype.push.apply(tasks, taskCategory.tasks));
        return tasks;
    }

    private reflectTaskToParentPage() {
        var tasks = this.cloneObj(this.getAllTasksFromTaskCategory());
        this.data.selectedBpmn.tasks.length = 0;
        Array.prototype.push.apply(this.data.selectedBpmn.tasks, tasks)
    }

    private setupTaskCategory(tasks) {//extend taskCategory with necessary fields
        this.taskCategories.forEach(taskCategory => {
            if (taskCategory.type !== 'main') {
                taskCategory.formGroup = this.createNewTaskFormGroup()
            }
            this.initRoles(taskCategory);
            taskCategory.tasks = tasks.filter(task => task.type === taskCategory.type)
            taskCategory.assignees = {};
        });
    }

    private createNewTaskFormGroup() {
        var roleControl = new FormControl('', [Validators.required]);
        var assigneeControl = new FormControl({ value: '', disabled: true }, [Validators.required]);
        roleControl.valueChanges.subscribe((value) => {
            if (value) {
                assigneeControl.enable({ onlySelf: true });
            } else {
                assigneeControl.disable({ onlySelf: true });
            }
        })
        return new FormGroup({
            name: new FormControl('', [Validators.required]),
            role: roleControl,
            assignee: assigneeControl
        })
    }

    private initRoles(taskCategory) {//for each task type fetch roles;
        this.bpmnService.getRolesByTaskType(taskCategory.type).subscribe(
            data => taskCategory.roles = data,
            error => this.toastService.error('An error occurred while fetching roles. Please try again or contact administrator.')
        );
    }

    ///TODO: this fn to be moved to common utility
    /**for each items holding saved object value, assign it to matching reference from options 
     * so that both object matches and automatically be shown as selected value in dropdown.
     * It will help to get rid off compareWith function for mat-select
     * @param(options) : [{id:1, name:'xyz'},...]
     * @param(items) : Array or Object [{id:1, selectedValue:{id:1, name:'xyz'},someOtherprop:1},...]
     * @param(dataKey) : equals to 'selectedValue' for above example
     * @param(idKey) : equals to 'id' for above example as it will be compared for object equality
     * */
    // private assignRefForDropdowns(options, items, dataKey, idKey) {
    //   if (!Array.isArray(items)) {
    //     items = [items];
    //   }
    //   items.forEach(containerObj => {//containerObj[dataKey] is the selected/saved value; {idKey:123,...}
    //     containerObj[dataKey] = containerObj[dataKey] && options.find(option => option[idKey] === containerObj[dataKey][idKey]);
    //   });
    // }

    compareAssigneeFn(option, selection): boolean {
        console.log(option, selection)
        return option && selection ? option._id === selection._id : option === selection;
    }

    panelOpen: boolean; activePanel: string;
    onAssigneePanelOpen(panelOpen, matSelectAssignee, taskCategory, role, newOrExisting: string, rowIndex) {
        this.panelOpen = panelOpen;
        if (panelOpen) {
            this.activePanel = this.getUniquePanelId(taskCategory, newOrExisting, rowIndex);
            this.initAssignees(taskCategory, role, matSelectAssignee);
        }
    }

    getUniquePanelId(taskCategory, newOrExisting, rowIndex?) {
        return taskCategory.type + newOrExisting + rowIndex;
    }

    canUseNormalAssigneeDropdown(taskCategory, role, newOrExisting, rowIndex) {
        return !this.panelOpen || taskCategory.assignees[role] || this.activePanel !== this.getUniquePanelId(taskCategory, newOrExisting, rowIndex);
    }

    /* onRoleSelectionChange(matSelectChange, taskCategory) {
        //fetching assignee on role change will give best user experience but it adds complexity in error scenario.
        this.initAssignees(taskCategory, matSelectChange.value)
    } */

    private initAssignees(taskCategory, role, matSelectAssignee?) {//for given role under given task type, fetch the assignees
        if (taskCategory.assignees[role]) {
            console.log(`assignees for role ${role} of task type ${taskCategory.name} already fetched earlier`);
            return;
        }
        this.bpmnService.getAssignees(taskCategory.type, role).subscribe(
            data => {
                taskCategory.assignees[role] = data;
                // this.assignRefForDropdowns(data, taskCategory.tasks, 'assignee', '_id')//its fully working but switching to mat-select compareWith
            },
            error => {
                this.toastService.error('An error occurred while fetching assignees. Please try again or contact administrator.')
                matSelectAssignee && matSelectAssignee.close()
            }
        );
    }

    save() {
        this.saving = true;
        this.bpmnService.saveTask({}).subscribe(
            data => {
                this.toastService.success('Data saved successfully.')
                this.dialogRef.close();
            },
            error => {
                this.saving = false
                this.toastService.error('An error occurred while saving the data. Please try again or contact administrator.')
            }
        );
    }

    add(taskCategory) {
        var formGroup = taskCategory.formGroup;
        var task = formGroup.value;
        task.type = taskCategory.type;
        taskCategory.tasks.push(task);
        formGroup.reset();
        this.reflectTaskToParentPage();
    }
    isDataChanged;
    setAssignee() {
        this.isDataChanged = true;
    }

    delete(item, items) {

        if (!item.id) {//local item
            return items.splice(items.indexOf(item), 1);
        }

        item.deleting = true;
        this.bpmnService.deleteTask(item).subscribe(
            data => {
                items.splice(items.indexOf(item), 1);
                this.toastService.success('Item deleted successfully.')
            },
            error => {
                item.deleting = false
                this.toastService.error('An error occurred while deleting the data. Please try again or contact administrator.')
            }
        );
    }

}

