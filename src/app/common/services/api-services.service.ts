import { Injectable } from '@angular/core';

@Injectable()
export class ApiServicesService {
    constructor() {
    }

    /** 
      * Function to get base url for api on live server, will use static api url on local server.
      * Return api url
    **/
    public getApiBaseUrl() {
        if (window.location.hostname != 'localhost') {
            var href = window.location.href;
            var dir = href.substring(0, href.lastIndexOf('/')) + "/";
            var baseApiPath = dir.substring(0, dir.length - 2);
            return baseApiPath;
        }
        else {
            var apiUrl = 'http://ec2-52-88-156-207.us-west-2.compute.amazonaws.com:8080/bw-dot-1.01/';
            return apiUrl;
        }
    }

    public getApiBaseUrlDirectoryName() {
        if (window.location.hostname != 'localhost') {
            var href = window.location.href;
            var dir = href.substring(0, href.lastIndexOf('/')) + "/";
            var baseApiPath = dir.substring(0, dir.length - 2);
            return this.checkString(baseApiPath);
        }
        else {
            //var apiUrl = 'http://ec2-52-88-156-207.us-west-2.compute.amazonaws.com:8080/bw-dot-1.01/';
            var apiUrl = 'http://52.88.156.207:8080/bw-dot-1.01/'
            return apiUrl;
        }
    }

    checkString(stringToCheck) {
        var list1 = stringToCheck.split('/');
        if (list1[list1.length - 1].length < 1) {
            var item = list1[list1.length - 2]
            return item;
        } else {
            var item = list1[list1.length - 1];
            return item;
        }
    }
}
