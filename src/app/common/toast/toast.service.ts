import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material';
import { ToastComponent } from './toast.component';

@Injectable()
export class ToastService {
  private verticalPosition: MatSnackBarVerticalPosition = 'top'
  private horizontalPosition: MatSnackBarHorizontalPosition = 'center'
  private duration = 5000

  constructor(private snackBar: MatSnackBar) { }

  success(message: string) {
    return this.openSnackBar({
      message: message,
      icon: 'check_circle',
      title: 'Success',
      className: 'alert-snack-success'
    })
  }

  error(message: string = 'An error occurred while processing the request. Please try again or contact administrator.') {
    return this.openSnackBar({
      message: message,
      icon: 'error',
      title: 'Error',
      className: 'alert-snack-error'
    })
  }

  warning(message: string) {
    return this.openSnackBar({
      message: message,
      icon: 'warning',
      title: 'Warning',
      className: 'alert-snack-warning'
    })
  }

  info(message: string) {
    return this.openSnackBar({
      message: message,
      icon: 'info',
      title: 'Info',
      className: 'alert-snack-info'
    })
  }

  notify(message: string) {
    return this.snackBar.open(message, null, {
      duration: this.duration,
      verticalPosition: this.verticalPosition,
      horizontalPosition: this.horizontalPosition
    });
  }

  private openSnackBar(data: any) {
    return this.snackBar.openFromComponent(ToastComponent, {
      data: data,
      panelClass: 'alert-snack-bar-container',
      duration: this.duration,
      verticalPosition: this.verticalPosition,
      horizontalPosition: this.horizontalPosition
    });
  }

}
