import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';
import { HttpClient } from "@angular/common/http";
import { ApiServicesService } from '../common/services/api-services.service';

@Injectable()
export class DashboardService {

    constructor(private http: HttpClient, private _apiServicesService: ApiServicesService) { }
    public getDashboardEntries() : Observable<any[]>{
        return this.http.get(this._apiServicesService.getApiBaseUrl() + 'baf/GetDashboardEntries')
        .map(res => { 
            console.log("DashboardEntries: "+ res);            
            return res;
        })
        .catch(this.handleErrorObservable);
    }

    /** 
      * Function to handle exception from Api response.
    **/
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    /** 
     * Function to handle exception from Api response.
    **/
    private handleErrorObservable(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }
}
