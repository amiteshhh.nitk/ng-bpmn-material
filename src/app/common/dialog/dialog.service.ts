import { Injectable } from '@angular/core';

import { MatDialog } from '@angular/material'

import { DialogComponent } from './dialog.component';

@Injectable()
export class DialogService {
  constructor(private dialog: MatDialog) { }
  DEFAULTS = {
    autoFocus: false,
    minWidth: '250px',
    maxWidth: '500px',
    okText: 'OK',
    okClass: 'mat-primary',
    cancelText: 'Cancel',
    cancelClass: 'mat-accent',
    deleteText: 'Delete',
    deleteClass: 'mat-warn',
    spinnerText: 'Please wait...'
  }
  alert(userOption: any, config?: any) {
    return this.open(this.createOptions([{
      text: userOption.okText || this.DEFAULTS.okText,
      className: userOption.okClass || this.DEFAULTS.okClass
    }], userOption));
  }
  confirm(userOption: any, config?: any) {
    return this.open(this.createOptions([{
      text: userOption.cancelText || this.DEFAULTS.cancelText,
      className: userOption.cancelClass || this.DEFAULTS.cancelClass
    }, {
      text: userOption.okText || this.DEFAULTS.okText,
      className: userOption.okClass || this.DEFAULTS.okClass
    }], userOption))
  }
  delete(userOption: any, config?: any) {
    return this.open(this.createOptions([{
      text: userOption.cancelText || this.DEFAULTS.cancelText,
      className: userOption.cancelClass || this.DEFAULTS.cancelClass
    }, {
      text: userOption.okText || this.DEFAULTS.deleteText,
      className: userOption.deleteClass || this.DEFAULTS.deleteClass
    }], userOption))
  }
  spin(text?: string) {
    var options = {
      spin: true,
      text: text === undefined ? this.DEFAULTS.spinnerText : text,
      disableClose: true
    };
    if (!options.text) {
      // options['height'] = '70px';
      options['panelClass'] = 'spinner-dialog-round';
    }
    return this.open(options);
  }
  open(userOption: any) {
    userOption.minWidth = userOption.minWidth || this.DEFAULTS.minWidth;
    userOption.maxWidth = userOption.maxWidth || this.DEFAULTS.maxWidth;
    userOption.data = userOption;
    userOption.autoFocus = this.DEFAULTS.autoFocus;
    return this.dialog.open(DialogComponent, userOption);
  }
  private createOptions(buttons, userOption) {
    var options;
    if (typeof userOption === 'string') {
      options = { text: userOption }
    } else {
      options = userOption;
    }

    options.buttons = buttons;
    options.position = {
      top: '64px'
    };
    return options;
  }
  // private copy(obj) {
  //   return JSON.parse(JSON.stringify(obj))
  // }
  // private extend(obj1, obj2) {
  //   //return JSON.parse(JSON.stringify(obj))
  // }
}

