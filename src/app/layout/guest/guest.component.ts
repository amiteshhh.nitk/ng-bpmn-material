import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.component.html',
  styleUrls: ['./guest.component.scss']
})
export class GuestComponent implements OnInit {
  menus = [
    { path: '/about', title: 'About', icon: 'info' },
    { path: '/contact', title: 'Contact Us', icon: 'contact_support' },
    { path: '/login', title: 'Login', icon: 'input' }
  ]
  
  constructor() { }

  ngOnInit() {
    
  }

  onMenuClick(menu) {
    console.log(menu);
  }

}
