import {//add 3 items per row for beautification and readability
  MatButtonModule, MatCheckboxModule, MatCardModule,
  MatToolbarModule, MatTabsModule, MatIconModule,
  MatListModule, MatSidenavModule, MatTableModule,
  MatInputModule, MatFormFieldModule, MatTooltipModule,
  MatProgressSpinnerModule, MatSortModule, MatDialogModule, 
  MatSelectModule, MatMenuModule, MatSnackBarModule,
  MatDatepickerModule, MatNativeDateModule, MatRadioModule
} from '@angular/material';

import { LayoutModule } from '@angular/cdk/layout'
import { NgModule } from '@angular/core';

var modules = [//add 3 items per row for beautification and readability
  LayoutModule, MatSortModule, MatDialogModule,
  MatButtonModule, MatCheckboxModule, MatCardModule,
  MatToolbarModule, MatTabsModule, MatIconModule,
  MatListModule, MatSidenavModule, MatTableModule,
  MatInputModule, MatFormFieldModule, MatTooltipModule,
  MatProgressSpinnerModule, MatSelectModule, MatMenuModule,
  MatSnackBarModule, MatDatepickerModule, MatNativeDateModule,
  MatRadioModule
]

@NgModule({
  imports: modules,
  exports: modules
})
export class AppMaterialModule { }