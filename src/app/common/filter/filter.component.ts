import { Component, OnInit, Input, Host } from '@angular/core';

import { MatColumnDef, MatTable } from '@angular/material';

import { FilterService, IFilterSetting } from './filter.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  @Input() column: string
  @Input() filterType: string
  dataSource
  filterSetting: IFilterSetting
  applyFilter
  constructor(@Host() matTable: MatTable<any>, @Host() matColumnDef: MatColumnDef, filterService: FilterService) {

    this.dataSource = matTable.dataSource;
    this.filterSetting = this.dataSource['filterSetting'];
    if (!this.column) {
      this.column = matColumnDef.name;
    }
    if (!this.filterType && this.filterSetting.filterParams && this.filterSetting.filterParams[this.column]) {
      this.filterType = this.filterSetting.filterParams[this.column].filterType;
    }
    this.applyFilter = () => filterService.applyFilter(this.dataSource);
  }

  ngOnInit() {

  }

  isAllSelected() {
    var selectedValues = this.filterSetting.filterVals.columns[this.column];
    var values = this.filterSetting.filterParams[this.column].values;
    return selectedValues && values && selectedValues.length === values.length
  }

  masterToggle(matSelectionListRef) {
    // if (this.isAllSelected()) {
    if (matSelectionListRef.selectedOptions.selected.length) {//clearnig out seems more natural here hence commented above line
      matSelectionListRef.deselectAll();
    } else {
      matSelectionListRef.selectAll();
    }
  }

}
