import { Component, TemplateRef, ViewChild } from "@angular/core";
// import BpmnViewer from "bpmn-js";
import Viewer from 'bpmn-js/lib/Viewer'; 
//declare var BpmnViewer: any;
// const BpmnViewer = require('module-name');
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";

//move to bpmn svc
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
//import { of } from 'rxjs/observable/of';
//import 'rxjs/add/observable/of';
import { catchError, map, tap } from "rxjs/operators";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "CodeSandbox";
  bsModalRef: BsModalRef;
  bpmnElement: any;
  isPopupOpen= false;
  isTooltipOpen= false;

  libs=[{name: 'BPMN', version: '2.4.0'}, {name: 'Angular CLI', version: '1.7.3'}, {name: 'Angular', version: '5.5.1'}, {name: 'ngx-bootstrap', version: '3.3.1'}
, {name: 'bootstrap css', version: '4.1.2'}]
  
  @ViewChild("bmpnInfoTemplate") bmpnInfoTemplate: TemplateRef<any>;
  private viewer: any;
  constructor(private modalService: BsModalService, private http: HttpClient) {}
  ngOnInit() {
    debugger;
    //var xml = this.getStaticXml(); // my BPMN 2.0 xmls
    this.viewer = new Viewer({
      container: "#bpmnDiagram"
    });
    this.loadBpmnDiagram(this.files[2]);
  }

  files= ['pizza-collaboration.bpmn', 'bpmn-demo-sample.bpmn', 'bpmn-actual-sample.bpmn']
  activeFile: string
  loadBpmnDiagram(file:string){
    this.activeFile = file;
    this.getBpmnXml(file).subscribe(xml => {
      this.showBpmnDiagram(xml);
      this.hookEventOnBpmn();
    });
  }

  getBpmnXml(file:string): Observable<any> {
    return this.http.get("assets/" + file, { responseType: "text" }).pipe(
      tap(bpmnSampleXmlStr => console.log("fetched bpmn sample xml file"))
      //,
      // catchError(this.handleError("getBpmnXml", null)
      // )
    );
  }

  private showBpmnDiagram(xml) {
    this.viewer.importXML(xml, function(err) {
      if (err) {
        console.log("error rendering", err);
      } else {
        console.log("rendered");
      }
    });
  }

  private hookEventOnBpmn() {
    var eventBus = this.viewer.get("eventBus");
    //var allowedTypes= ['Shape', 'Label'];
    var allowedTypes= ['Shape'];
    eventBus.on("element.click", e => {
      // e.element = the model element
      // e.gfx = the graphical element
      console.log("clicked element", e.element);
      e.stopPropagation();//in bpmn 2.4 its firing twice
      var nodeType = e.element.constructor.name;
      //show as popover
      // this.isPopupOpen = this.isTooltipOpen = allowedTypes.includes(nodeType);
      // this.openPopover(e.element);
      //show as modal
      if(allowedTypes.includes(nodeType)){
        this.openModal(this.bmpnInfoTemplate, e.element);
      }
      
      //this.openModalWithComponent(e.element);
    });
  }
  bpmnTitle: string;
  bpmnContent: string;
  openPopover(bpmnElement: any){
    this.bpmnTitle = bpmnElement.constructor.name;
    this.bpmnContent = 'Id of this element is' + bpmnElement.id;
  }
  openModal(template: TemplateRef<any>, bpmnElement: any) {
    this.bpmnElement = bpmnElement; //expose it to ng-template
    this.bsModalRef = this.modalService.show(template);
    //this.modalRef = this.modalService.show(template, initialState);
  }
  // openModalWithComponent(bpmnElement) {
  //   const initialState = {
  //     bpmnElement: bpmnElement
  //   };
  //   this.bsModalRef = this.modalService.show(ModalContentComponent, { initialState });
  //   this.bsModalRef.content.closeBtnName = 'Close';
  // }
  
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  // private handleError<T>(operation = "operation", result?: T) {
  //   return (error: any): Observable<T> => {
  //     // TODO: send the error to remote logging infrastructure
  //     console.error(error); // log to console instead
  //     // TODO: better job of transforming error for user consumption
  //     console.log(`${operation} failed: ${error.message}`);
  //     // Let the app keep running by returning an empty result.
  //     return of(result as T);
  //   };
  //}
}