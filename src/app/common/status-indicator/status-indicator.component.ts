import { Component, OnInit, Input } from '@angular/core';

import { StatusColorService } from './status-color.service';

@Component({
  selector: 'app-status-indicator',
  templateUrl: './status-indicator.component.html',
  styleUrls: ['./status-indicator.component.scss']
})
export class StatusIndicatorComponent implements OnInit {
  @Input() type;
  @Input() status;
  bgColor;

  constructor(private statusColorService: StatusColorService) { }

  ngOnInit() {
    this.bgColor = this.statusColorService.getColor(this.status, this.type) 
    console.log(this.status, this.bgColor)
  }

}
