import { Component, OnInit, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { MatDialog } from '@angular/material'
import Viewer from 'bpmn-js/lib/NavigatedViewer';
// import Viewer from 'bpmn-js/lib/Viewer';

import { BpmnService } from '../bpmn.service'
import { ToastService } from './../../common/toast/toast.service';
import { StatusColorService } from './../../common/status-indicator/status-color.service';

import { BpmnVariableComponent } from '../variable/bpmn-variable.component'
import { BpmnTaskComponent } from './../task/bpmn-task.component';

var self//this is needed as `this` context will change in non-angular context like bpmn events callback
@Component({
  selector: 'app-bpmn-diagram',
  templateUrl: './bpmn-diagram.component.html',
  styleUrls: ['./bpmn-diagram.component.scss']
})
export class BpmnDiagramComponent implements OnInit {
  params; saving = false; isPageLoaded : boolean; selectedBpmn; dialogRef; selectedItemType; subTasks: Array<any>;
  //elementType will decide what to show among following: property list popup, task hover details, task list popup, timer duration popup, sub process bpmn load
  private allData: any;//service response
  private bpmnElements: Array<any>;
  private bpmnViewer: any;
  private bpmnOverlays: any;
  @ViewChild('durationTemplateRef') durationTemplateRef: TemplateRef<any>;
  @ViewChild('taskAndDocumentTemplateRef') taskAndDocumentTemplateRef: TemplateRef<any>;
  @ViewChild('taskHoverElRef') taskHoverElRef: ElementRef;



  bpmnData = { xml: null, timers: null, variables: null, activities: null, calls: null, admin_person_id: null, phase_status: null }

  constructor(activatedRoute: ActivatedRoute, private router: Router, private bpmnService: BpmnService, private dialog: MatDialog, private toastService: ToastService, private statusColorService: StatusColorService) {
    self = this;
    this.params = activatedRoute.snapshot.params;
    this.router.routeReuseStrategy.shouldReuseRoute = function () {//required to reload the same component on route param change
      return false;
    };
  }

  ngOnInit() {
    this.isPageLoaded = false;
    this.bpmnService.getBpmnDetails(this.params.bpmnId).subscribe(
      data => {
        this.isPageLoaded = true;
        this.allData = data;
        this.bpmnViewer = new Viewer({
          container: "#bpmnDiagram"
        });
        this.renderBpmnDiagram(data);
        this.setAuth();

      }, // success path
      error => {
        this.isPageLoaded = true;
        this.toastService.error()
      }
    );
  }

  private setAuth() {
    var canEdit = this.allData.phase_status === 'defined' ///TODO cond && this.phase.is_managed
    this.allData.canEdit = canEdit;
  }

  private renderBpmnDiagram(data) {
    this.bpmnViewer.importXML(data.xml, function (err) {
      if (err) {
        self.toastService.error('An error occured while rendering BPMN diagram.')
        console.log("error rendering", err);
      } else {
        self.onBpmnRender(data);//called in this way to hook back `this` to this class component
      }
    });
  }

  private onBpmnRender(data) {
    var canvas = this.bpmnViewer.get('canvas');
    canvas.zoom('fit-viewport');

    this.bpmnOverlays = this.bpmnViewer.get('overlays');

    this.annotateBPMN(this.allData.timers, 36);
    this.annotateBPMN(this.allData.activities, 100);
    this.annotateBPMN(this.allData.calls, 100);

    this.bpmnElements = this.allData.timers.concat(this.allData.activities, this.allData.calls);
    this.hookEvent();
  }

  private annotationOverlayHtml(bpmnElement) {
    var borderInfo = bpmnElement.on_critical_path ? '3px solid red' : '1px solid black'
    return '<div style="background-color:' + this.statusColorService.getColor(bpmnElement.status, 'phase') + '; white-space:nowrap; padding:3px;border:' + borderInfo + ';border-radius:5px; text-align:center; font-size: x-small; width:50px;">' + bpmnElement.duration + '</div>';
  }

  private annotateBPMN(bpmnElements, width) {
    bpmnElements.forEach(bpmnElement => {
      this.bpmnOverlays.add(bpmnElement.bpmn_id, {
        position: {
          top: -22,
          left: (width - 60) / 2
        },
        html: this.annotationOverlayHtml(bpmnElement)
      });
    });
  }

  private hookEvent() {
    var events = [
      'element.click',
      'element.dblclick',
      'element.hover'
    ];
    // this.processActivities.forEach(item =>//target event attachment
    //   document.querySelector('[data-element-id=' + item.bpmn_id + ']').addEventListener('click', () => alert(1))
    // );

    var eventBus = this.bpmnViewer.get('eventBus');
    // eventBus.on('element.dblclick', 999999,  event => self.eventHandler(event));
    events.forEach((event) => {
      eventBus.on(event, event => self.eventHandler(event));
    });
  }

  private eventHandler(event) {
    var element = event.element;
    // this.activeBpmnNode = element;
    console.log(event.type, element)
    this.selectedItemType = null;
    event.stopPropagation();//in bpmn 2.4 its firing twice
    switch (event.type) {
      case "element.hover":
        this.onNodeHover(element);
        break;
      case "element.click":
        this.onNodeClick(element);
        break;
      case "element.dblclick":
        this.doubleClickElement(element);
        break;
    }
  }

  private findBpmnElement(bpmnNode) {
    return this.bpmnElements.find(item => item.bpmn_id === bpmnNode.id);
  }

  /* ********** START HOVER IMPLEMENTATION ***************  */
  // hoverOverlayId = null;
  hoverOverlayIds = [];//sometime two instance of popover getting opened on single hover event especially in IE edge which leads to permanent task popover in the screen, therefore we are maintaining array
  private removeBpmnHoverInfo(skipBpmnReset?: boolean) {
    this.hoverOverlayIds.forEach((hoverOverlayId) => this.bpmnOverlays.remove(hoverOverlayId));
    this.hoverOverlayIds.length = 0;
    if (!skipBpmnReset) {
      this.selectedBpmn = null;
    }
  }
  private onNodeHover(element) {

    /* if (element.type === 'bpmn:Process') {//commented out as we can deduce the condition by date alone
      return;
    } */
    var currentBpmn = this.findBpmnElement(element);
    if (currentBpmn && currentBpmn === this.selectedBpmn) {//same element hover event fire again
      return;
    }
    this.removeBpmnHoverInfo();
    this.selectedBpmn = currentBpmn;
    if (!this.selectedBpmn || !this.selectedBpmn.start) {
      return;
    }
    if (this.selectedBpmn.tasks) {
      this.subTasks = this.selectedBpmn.tasks.filter((item) => item.type !== 'main');
    } else {
      this.subTasks = null;
    }
    this.selectedItemType = 'hover:task'
    setTimeout(() => {//lets wait for markup to be rendered bkz of *ngIf
      this.showTaskHover(element)
    }, 200);
  }

  private showTaskHover(element) {
    console.log('inside showTaskHover')
    if (!this.selectedBpmn) {//It will happening in rapid mouse movement
      //and in that case htmlStr would be just <!--bindings={ "ng-reflect-ng-if": null}-->
      return;
    }
    if (this.hoverOverlayIds.length) {//in rapid mouse movement sometime previous hover overlay not going and we see 2 overlays
      this.removeBpmnHoverInfo(true);
    }
    var htmlStr = this.taskHoverElRef.nativeElement.innerHTML;
    var hoverOverlayId = this.bpmnOverlays.add(element.id, {
      position: {
        top: -5,//minus value to hide the border of bpmn node
        right: -5
      },
      html: htmlStr
    });
    this.hoverOverlayIds.push(hoverOverlayId);
  }
  /* ********** END HOVER IMPLEMENTATION ***************  */
  /* ********** DOUBLE CLICK IMPLEMENTATION ***************  */
  private doubleClickElement(element) {
    this.removeBpmnHoverInfo();

    this.selectedBpmn = this.findBpmnElement(element);
    if (!this.selectedBpmn || !this.selectedBpmn.name || this.selectedBpmn.elementType !== 'subprocessCall') {
      return;
    }
    // this.router.navigateByUrl('/dummy-url-hack', { skipLocationChange: true });//https://stackoverflow.com/questions/47813927/how-to-refresh-a-component-in-angular
    this.router.navigate(['/bpmn/diagram', this.params.projectId, this.params.phaseId, this.selectedBpmn.bpmn_id], { queryParamsHandling: 'preserve' });
    //to change the text in breadcrumb we need to change queryparams

    // var dtIdx = $window.location.href.indexOf('bpmn?');
    // var href = $window.location.href.substring(0, dtIdx + 5);
    // var newHref = href + 'project_id=' + self.projectId + '&phase_id=' + self.phaseId + '&process=' + processName;
    // $window.location.href = newHref;
  }
  /* ********** END OF DOUBLE CLICK IMPLEMENTATION ***************  */
  /* ********** CLICK IMPLEMENTATION ***************  */

  private onNodeClick(element) {

    this.removeBpmnHoverInfo();

    this.selectedBpmn = this.findBpmnElement(element);
    // console.log('clicked', element.type, this.selectedBpmn.elementType, element)
    if (this.selectedBpmn && this.selectedBpmn.elementType === 'timer') {//show timer duration edit popup
      this.showTimerDurationDialog();
    } else if (this.selectedBpmn && this.selectedBpmn.elementType === 'activity') {//show Task edit popup
      this.showTaskDialog();
    } /* else if (this.selectedBpmn && this.selectedBpmn.elementType === 'subprocessCall' && element.type == 'bpmn:CallActivity') {
        //(double-click to display called process) 
      } */
    else if (element.type === 'bpmn:Process') {//show process variable edit popup
      this.showProcessVariableDialog();
    }
  }
  commonDialogPanelClass= ['edit-form-dialog', 'has-dialog-close-icon', 'lightBlue-bg-theme', 'dialog-container-padding-top-0']
  private showProcessVariableDialog() {
    if (!this.allData.variables.length) {
      return this.toastService.info('Oops! No process variable exists.');
    }
    this.selectedItemType = 'click:variable'
    this.dialog.open(BpmnVariableComponent, {
      panelClass: this.commonDialogPanelClass,
      // width: '600px',
      autoFocus: false,
      data: this.allData
    });
  }
  private showTaskDialog() {
    this.selectedItemType = 'click:task'
    this.dialog.open(BpmnTaskComponent, {
      // panelClass: this.commonDialogPanelClass,//use concat to add additional classes
      panelClass: this.commonDialogPanelClass,
      width: '1100px',
      position: { top: '100px' },
      data: {
        selectedBpmn: this.selectedBpmn,
        allData: this.allData
      },
      autoFocus: false
    }).afterClosed().subscribe(value=>{
        if(value){
          this.refreshPage();
        }
    });
  }

  private refreshPage(){//it can be reutilized with double click also but will require some modification
    // this.router.navigateByUrl(this.router.url);//this will also work but why to reload entire thing; just do the reinitilization of data
    this.bpmnViewer.destroy();
    this.ngOnInit()
  }

  private showTimerDurationDialog() {
    this.selectedItemType = 'click:timer'
    this.dialogRef = this.dialog.open(this.durationTemplateRef, {
      panelClass: this.commonDialogPanelClass,
      // panelClass: 'lightBlue-bg-theme',
      // autoFocus: false
    });
  }
  saveDuration() {
    this.saving = true;
    this.bpmnService.saveDuration({}).subscribe(
      data => {
        this.saving = false;//todo
        this.toastService.success('Data saved successfully.')
        this.dialogRef.close();
      },
      error => {
        this.saving = false
        this.toastService.error()
      }
    )
  }
}
