import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GuestComponent } from './layout/guest/guest.component'
import { DashboardLayoutComponent } from './layout/dashboard-layout/dashboard-layout.component'
import { HomeComponent } from './home/home.component'
import { AboutComponent } from './about/about.component'
import { ContactComponent } from './contact/contact.component'
import { LoginComponent } from './login/login.component'
import { DashboardComponent } from './dashboard/dashboard.component';
import { DocumentsComponent } from './documents/documents.component'
import { RfisComponent } from './rfis/rfis.component'
import { ProjectsComponent } from './projects/projects.component'
import { ProjectEditComponent } from './project-edit/project-edit.component'
import { PhasesComponent } from './phases/phases.component'
import { BpmnComponent } from './bpmn/bpmn.component'
import { BpmnDiagramComponent } from './bpmn/diagram/bpmn-diagram.component'
import { TasksComponent } from './tasks/tasks.component'
import { HelpComponent } from './help/help.component'
import { AuthGuardService } from './common/services/authguard.service'

const appRoutes: Routes = [//other top level route config goes here
  { path: '', redirectTo: '/bpmn/diagram/593cb50157fbc004536d8b2b/2/2?projectName=For%20DOT%20Testing&phaseName=2&bpmnName=2', pathMatch: 'full' },
  //{ path: '404', component: HomeComponent, data: { title: 'Home' } },
];

const guestRouteList: Routes = [
  { path: 'home', component: HomeComponent, data: { title: 'Home' } },
  { path: 'about', component: AboutComponent, data: { title: 'About' } },
  { path: 'contact', component: ContactComponent, data: { title: 'Contact Us' } },
  { path: 'login', component: LoginComponent, data: { title: 'Login', icon: 'contact' } }
];
const guestRouteParent: Routes = [
  { path: '', component: GuestComponent, children: guestRouteList }
];

const loggedInRouteList: Routes = [
  { path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard', breadcrumb: true } },
  { path: 'docs', component: DocumentsComponent, data: { title: 'Documents', breadcrumb: { parentPath: 'dashboard' } } },
  { path: 'rfi', component: RfisComponent, data: { title: 'RFIs', breadcrumb: { parentPath: 'dashboard' } } },
  { path: 'projects', component: ProjectsComponent, data: { title: 'Projects', breadcrumb: { parentPath: 'dashboard', valueKey: 'projectName' } } },
  // { path: 'project-edit', component: ProjectEditComponent, data: { title: 'Project Edit', breadcrumb: true } },
  { path: 'phases/:projectId', component: PhasesComponent, data: { title: 'Phases', breadcrumb: { parentPath: 'projects', valueKey: 'phaseName' } } },
  { path: 'bpmn/:projectId/:phaseId', component: BpmnComponent, data: { title: 'BPMN', breadcrumb: { parentPath: 'phases/:projectId', valueKey: 'bpmnName' } } },
  { path: 'bpmn/diagram/:projectId/:phaseId/:bpmnId', component: BpmnDiagramComponent, data: { title: 'Diagram', breadcrumb: { parentPath: 'bpmn/:projectId/:phaseId' } } },
  { path: 'tasks', component: TasksComponent, data: { title: 'Tasks', breadcrumb: { parentPath: 'dashboard' } } },
  { path: 'help', component: HelpComponent, data: { title: 'Help', breadcrumb: { parentPath: 'dashboard' } } }
];
const loggedInRouteParent: Routes = [
  { path: '', component: DashboardLayoutComponent, children: loggedInRouteList, canActivate: [AuthGuardService] }
];

// export const routeList: Routes = appRoutes.concat(guestRouteList, loggedInRouteList);


@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload' }), RouterModule.forChild(guestRouteParent), RouterModule.forChild(loggedInRouteParent)],
  exports: [RouterModule]
})
export class AppRoutingModule { }