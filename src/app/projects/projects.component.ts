import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSort, MatTableDataSource, MatDialog, MatDialogConfig } from '@angular/material';

import { ProjectsService } from './projects.service';
// import { getLocaleDateFormat } from '../../../node_modules/@angular/common';
// import { timeout } from '../../../node_modules/@types/q';
import {  FilterService, IFilterParams } from '../common/filter/filter.service';
import { MessageService } from '../common/services/message.service';
import { DialogService } from '../common/dialog/dialog.service';
import { MatSelectionModelHelper } from '../common/utils/MatSelectionModelHelper';
import { ProjectEditComponent } from '../project-edit/project-edit.component';

import { ToastService } from '../common/toast/toast.service';//added for illustration only.These methods are part of dialog svc as well

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  isPageLoaded;
  isSaving:boolean;
  filterSubscription;
  refreshSubscription;
  selection = new SelectionModel(true, []);
  dataSource = new MatTableDataSource();
  
  //displayedColumns = ['select', 'name', 'symbol'];
  displayedColumns: string[] = ['status', 'name', 'button_action'];
  statusClass = {
    defined: 'status-button-important',
    running: 'status-button-normal',
    ended: 'status-button-ended'
  }

  staticData = [
    { "_id" : "593cb50157fbc004536d8b2b", "status" : "defined", "name" : "For DOT Testing" }, 
    { "_id" : "5b7fc38157fbc00450fa5a5c", "status" : "defined", "name" : "august-24" }, 
    { "_id" : "5b7fc55957fbc00450fa5b07", "status" : "running", "name" : "for testing" }
  ]
  @ViewChild(MatSort) sort: MatSort;
  constructor(private projectsService: ProjectsService, private messageService: MessageService, private dialogService: DialogService, private router: Router, private dialog: MatDialog, private filterService: FilterService, private toastService: ToastService) {
    this.filterSubscription = this.messageService.on('app-filter-input').subscribe(item => this.applyFilter(item.data));
    this.refreshSubscription = this.messageService.on('app-page-refresh').subscribe(() => this.pageRefresh());
  }

  ngOnInit(isRefresh?: boolean) {
    this.initPageState(isRefresh);

    var self = this;
    setTimeout(function () { self.getAllData() }, 2000);

  }

  private pageRefresh() {
    this.ngOnInit(true);
  }

  private initPageState(isRefresh?: boolean) {//initialize or reset as if loaded first time
    this.isPageLoaded = false;
    if (!isRefresh) {
      this.createFilter();
    } else {
      this.filterService.clearFilterState(this.dataSource['filterSetting']);
      this.staticData.pop();
    }
  }

  getAllData() {
    this.isPageLoaded = true;
    this.dataSource.data = this.staticData;
    this.dataSource.sort = this.sort;
    this.dataSource.filter = null;
    this.filterService.refreshDropdownFilterValues(this.dataSource['filterSetting'], this.dataSource.data);
    
    //this.dataSource.filter = null;
    //this.selection.clear();
    // this.projectsService.getAll()
    //   .subscribe(items => {
    //     this.isPageLoaded = true;
    //     this.dataSource.filter = null;
    //     this.selection.clear();
    //     this.dataSource.data = items;
    //   });
  }

  isAllSelected() {
    return MatSelectionModelHelper.isAllSelected(this.selection, this.dataSource);
  }

  masterToggle() {
    MatSelectionModelHelper.masterToggle(this.selection, this.dataSource);
  }

  save(){
    this.isSaving = true;
  }

  cancel(){
    this.isSaving = false;
  }
  private applyFilter(quickFilterText: string) {
    this.filterService.applyFilter(this.dataSource, quickFilterText);
  }

  createFilter() {
    var filterParams :IFilterParams = {//not required for text filters
      status: {
        valueFormatter: this.filterService.initCap,
        filterType: 'dropdown',
        buildValuesFromData: true
      },
      name: {
        valueFormatter: this.filterService.initCap,
        filterType: 'dropdown',
        buildValuesFromData: true
      }
    }
    // console.log(this.displayedColumns.slice(0,2))
    return this.filterService.createFilter(this.dataSource, this.displayedColumns.slice(0,2), filterParams);//removing non-filterable columns
    // return this.filterService.createFilter(this.dataSource, this.displayedColumns, filterParams);//ideally remove the non-filterable columns from displayedColumns, currently last column is non-filterable but still its being passed
  }

  //dialog mockup
  //const initialSelection = [];
  dialogMsg = 'Selects all rows if they are not all selected; otherwise clear selection.'// Selects all rows if they are not all selected; otherwise clear selection. Selects all rows if they are not all selected; otherwise clear selection.'
  dialogMsgComplex = {
    title: 'Are you sure?',
    text: this.dialogMsg
  }
  openDialog(type, text): void {
    //let dialogRef = this.dialogService.open({ name: 'Amitesh' });
    //this.dialogService.spin();
    // let dialogRef = this.dialogService.alert('Selects all rows if they are not all selected; otherwise clear selection. Selects all rows if they are not all selected; otherwise clear selection. Selects all rows if they are not all selected; otherwise clear selection.');
    // let dialogRef = this.dialogService.confirm('Selects all rows if they are not all selected; otherwise clear selection. Selects all rows if they are not all selected; otherwise clear selection. Selects all rows if they are not all selected; otherwise clear selection.');

    let dialogRef = this.dialogService[type](text);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });
    if(type === 'spin'){
      setTimeout(()=>dialogRef.close(), 5000);
    }
  }
  openToast(type): void {
    var message = 'Selects all rows if they are not all selected; otherwise clear selection.'
    type = type || 'notify';
    this.toastService[type](message);//e.g. this.toastService.success(message)
    //this.dialogService[type](message);//for now this has been reverted to avoid confusion- Simple extend toast svc in dialog svc to make it work.. see log of dialog svc file
    //it returns MatSnackBarRef
  }
  //end of dialog mockup

  ngOnDestroy() {
    this.refreshSubscription.unsubscribe();
    this.filterSubscription.unsubscribe();
  }

  openProjectEditModal(id, name) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'edit-project-dialog-container';

    dialogConfig.data = {
      id: id,
      name: name
    };
    
    const dialogRef = this.dialog.open(ProjectEditComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => console.log("Dialog output:", data)
    ); 
  } 

}
