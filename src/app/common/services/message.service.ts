import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MessageService {
  private subjects = {};

  broadcast(event: string, data: any) {
    if (this.subjects[event]) {
      this.subjects[event].next({ event: event, data: data });
    }
  }

  on(event: string): Observable<any> {
    var subject = this.subjects[event];
    if (!subject) {
      subject = this.subjects[event] = new Subject();
    }
    return subject;
  }
}