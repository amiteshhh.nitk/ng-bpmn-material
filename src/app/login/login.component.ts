import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material'
import { LoginService } from './login.service';
import { DialogService } from '../common/dialog/dialog.service';
import 'rxjs/Rx';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  loading = false;
  userForm: any;
  closeResult: string;
  errorMessage: any;
  data;

  loginModel: any = {
    "email": null,
    "password": null,
  };

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
    private _loginservice: LoginService,
    public dialogRef: MatDialogRef<LoginComponent>,
    private dialogService: DialogService
  ) { }

  ngOnInit() {

  }

  submitLogin() {
    //Todo - add validation to check email & password before calling API
    if (this.loginModel.email && this.loginModel.password) {
      //this.openDialog('spin', 'Loading...');
      //this._loginservice.loginUser(this.loginModel).subscribe((data) => {
        var data = {
          "ui_hidden": true, "first_name": "Tester3",
          "document_filter_labels": [], "_id":
          "5acf50df4ac8efe4e19e91b3", "tz": "Asia/Kolkata",
          "menu_items": [{"name": "Documents", "url":
          "/docs", "icon": "documents"}, {"name": "RFIs",
          "url": "/rfi", "icon": "rfis"}, {"name":
          "Projects", "url": "/projects", "icon":
          "projects"}, {"name": "Tasks", "url": "/tasks",
          "icon": "tasks"}, {"name": "Help", "url": "/help",
          "icon": "help"}], "last_name": "Tester", "roles":
          ["BW-Test", "BW-File-Uploads"], "project_ids": []
        }
        //Todo - add inscope validation to check valid response before navigating to dashboard
        if (data['first_name'] || data['last_name']) {
          // localStorage.setItem('ui-hidden', data['ui_hidden']);
          // localStorage.setItem('document-filter-labels', data['document_filter_labels']);
          // localStorage.setItem('id', data['_id']);
          // localStorage.setItem('tz', data['tz']);
          // localStorage.setItem('menu-items', data['menu_items']);
          // localStorage.setItem('roles', data['roles']);
          // localStorage.setItem('project-ids', data['project_ids']);
          localStorage.setItem('first-name', data['first_name']);
          localStorage.setItem('last-name', data['last_name']);
          localStorage.setItem('currentUser', JSON.stringify(data));
          this.router.navigate(['/dashboard']);
          this.dialogRef.close();
        } else {
          //alert("Invalid Email or Password");
          this.openDialog('alert', 'Invalid Email or Password');
        }
      // },
      // error => {
      //   console.log(error);
      //   //alert("Something went wrong, contact site administrator");
      //   this.openDialog('alert', 'Something went wrong, contact site administrator');
      // });
    }
  }

  openDialog(type, text): void {
    let dialogRef = this.dialogService[type](text);
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });
  }

}
