import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuardService implements CanActivate 
{
  constructor(private router: Router) 
  {

  }
  canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean 
  {
    var firstname= localStorage.getItem('first-name');
    //console.log("MY AUTHGUARD",firstname);
    if (firstname!=null  || 1) 
    {
      // logged in so return true
      return true;
    }
    else
    {
      this.router.navigate(['/home']);
    }
  }
}
