import { Injectable } from '@angular/core';

@Injectable()
export class StatusColorService {

  constructor() { }
  getColor(status: string, type?: string) {
    type = type || 'phase';
    return this.statusColors[type][status] || this.defaultColors[status];
  }

  private defaultColors = {//value should be valid colour name/hex val
    defined: '#FFAB00',
    waiting: '#DD2C00',
    waiting2: 'Pink',
    started: 'Lime',
    running: '#00C853',
    ended: '#A9A9A9'
  }

  private phaseColors = {//specific color for phases
    running: 'Lime',//overriding default running for the sake of example
  }

  private projectColors = {//specific color for phases
  }

  private statusColors = {
    phase: this.phaseColors,
    project: this.projectColors
  }

}
