import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-phases',
  templateUrl: './phases.component.html',
  styleUrls: ['./phases.component.scss']
})
export class PhasesComponent implements OnInit {
  params: any;
  constructor(activatedRoute: ActivatedRoute) {
    this.params = activatedRoute.snapshot.params;
    // activatedRoute.paramMap.subscribe((paramsAsMap) => {
    //   this.projectId = paramsAsMap.get('projectId');
    // })
  }

  ngOnInit() {
  }

}
