import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { MediaMatcher } from '@angular/cdk/layout';
import { DomSanitizer } from "@angular/platform-browser";
import { MatIconRegistry } from "@angular/material/icon";
import { Router, NavigationEnd, ActivatedRoute, Route } from '@angular/router';

//rxjs/operators: Contains all pipeable operators.
import { map, filter } from 'rxjs/operators';

import { LoginService } from '../../login/login.service';
import { MessageService } from '../../common/services/message.service';


@Component({
  selector: 'app-dashboard-layout',
  templateUrl: './dashboard-layout.component.html',
  styleUrls: ['./dashboard-layout.component.scss']
})
export class DashboardLayoutComponent implements OnInit {
  mobileQuery: MediaQueryList;
  appSidenavClosed = false;
  menus: Array<any>;
  currentUser;
  breadcrumb = {
    items: [],
    queryParams: null,
    params: null
  };
  navEndSubscription;
  searchInputModel;

  constructor(media: MediaMatcher, private domSanitizer: DomSanitizer, private matIconRegistry: MatIconRegistry, private router: Router, private activatedRoute: ActivatedRoute, private loginService: LoginService, private messageService: MessageService, private location: Location) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.subscribeNavEndEvent();
  }
  ngOnInit() {
    this.currentUser = this.loginService.userDetails || JSON.parse(localStorage['currentUser']);//why 2 source of truth
    this.loadAppIcons();
    this.constructMenus();

  }

  private loadAppIcons() {
    var iconList = ['documents', 'rfis', 'tasks', 'projects', 'profile', 'help', 'menu', 'logout'];
    iconList.forEach((iconName) => {
      this.matIconRegistry.addSvgIcon(
        iconName,
        this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/' + iconName + '.svg')
      );
    });
  }

  private constructMenus() {
    this.menus = this.currentUser['menu_items'].map(item => (
      { path: item.url, title: item.name, icon: item.icon }
    ));
  }

  private subscribeNavEndEvent() {
    this.navEndSubscription = this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route) => {
        while (route.firstChild) route = route.firstChild;
        return route;
      }),
      filter((route) => route.outlet === 'primary')).subscribe((route) => {
        var data = { route: route, router: this.router };
        //this.messageService.broadcast('app-nav-end', data);//use this event for rest of the pages
        this.searchInputModel = '';
        this.constructBreadcumb(data);
      })
  }

  private constructBreadcumb(param) {//it assumes that every route will have breadcrumb
    var breadcrumbItems = [], parentRouteConfig: Route = param.route.routeConfig;
    var allRoutes: Array<Route> = param.route.parent.routeConfig.children;

    do {
      breadcrumbItems.unshift({
        name: parentRouteConfig.data.title,
        value: parentRouteConfig.data.breadcrumb.valueKey ? param.route.snapshot.queryParams[parentRouteConfig.data.breadcrumb.valueKey] : null,
        path: '/' + parentRouteConfig.path
      });
      parentRouteConfig = allRoutes.find((item) => item.path === parentRouteConfig.data.breadcrumb.parentPath)
    } while (parentRouteConfig)

    breadcrumbItems[breadcrumbItems.length - 1].value = null;//so that last item will not show the dynamic text

    this.breadcrumb = {
      items: breadcrumbItems,
      params: param.route.snapshot.params,
      queryParams: param.route.snapshot.queryParams
    };
  }

  trackBreadcrumb(index, item) {
    return item.path;
  }

  // onBreadcrumbClick(breadcrumb, clickedIndex) {//its working but not required
  //   event.preventDefault();
  //   var length = this.breadcrumb.items.length;
  //   if (clickedIndex === length) {
  //     return;
  //   }
  //   this.router.navigate([breadcrumb.path, this.breadcrumb.params], { queryParams: this.breadcrumb.queryParams, queryParamsHandling: 'preserve' });
  // }

  toggle(snav) {
    if (this.mobileQuery.matches) {
      snav.toggle();
    }
    this.appSidenavClosed = !this.appSidenavClosed;
  }

  applyFilter() {
    var minSearchCharLength = 2; //one less than actual
    var filterValue = this.searchInputModel.trim();//filter does case insensitive match
    if (filterValue.length === 0 || filterValue.length > minSearchCharLength) {
      this.messageService.broadcast('app-filter-input', filterValue);
    }
  }


  refresh() {
    this.searchInputModel = '';
    this.messageService.broadcast('app-page-refresh', null);
    //this.router.navigate([this.router.url]);
  }

  logoutUser() {
    localStorage.clear();
    this.router.navigate(['/home']);
    // this.authenticationService.logout()
    //   .subscribe(data => {
    //     console.log(data);
    //     localStorage.clear();
    //     this.router.navigate(['/home']);
    //   },
    //   error => {
    //     console.log(error);
    //     localStorage.clear();
    //     this.router.navigate(['/home']);
    //   });
  }

  ngOnDestroy() {
    this.navEndSubscription.unsubscribe();
  }

}
