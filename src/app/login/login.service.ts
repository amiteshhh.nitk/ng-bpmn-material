import { Injectable } from '@angular/core';
import { ApiServicesService } from '../common/services/api-services.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginService {
    userDetails:{};
    constructor(private http: HttpClient, private _apiServicesService: ApiServicesService) { }
    public loginUser(loginDetails) : Observable<any[]>{
        return this.http.post(this._apiServicesService.getApiBaseUrl() + '/etc/LoginPost', loginDetails)
        .map(res => { 
            console.log("Login Details: "+ res);            
            return res;
        })
        .catch(this.handleErrorObservable);
    }

    /** 
     * Function to handle exception from Api response.
    **/
    private handleErrorObservable(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }

}