import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ToastService } from '../../common/toast/toast.service';
import { BpmnService } from './../bpmn.service';
import { getLocaleEraNames } from '@angular/common';

@Component({
  selector: 'app-bpmn-document',
  templateUrl: './bpmn-document.component.html',
  styleUrls: ['./bpmn-document.component.scss']
})
export class BpmnDocumentComponent implements OnInit {
  documents: Array<any>; saving = false; file: any
  @ViewChild('formRef') formRef: ElementRef;
  @ViewChild('docNameRef') docNameRef: ElementRef;

  labels = ['Guide', 'Help Doc'];

  myFormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    labels: new FormControl('', [Validators.required]),
    mandatory: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required])
  })

  constructor(private toastService: ToastService, private bpmnService: BpmnService) { }

  ngOnInit() {
    this.documents = [{
      id: 1,
      name: 'Help document 1',
      labels: 'Guide',
      mandatory: 'No',
      contentType: 'Word doc',
      // description: 'Template driven forms.'
    }, {
      id: 2,
      name: 'Help document 2',
      labels: 'Help, Guide, Important',
      mandatory: 'Yes',
      contentType: 'Word doc',
      description: 'Template driven forms are forms where we write logic\n, validations\n, controls\n etc, in the template part of the code (html code). The template is responsible for setting up the form, the validation, control, group etc. Template driven forms are suitable for simple scenarios, uses two way data binding using the [(NgModel)] syntax, easier to use though unit testing might be a challenge.'
    }];
  }

  onFileSelect($event) {
    console.log('change')
    this.file = $event.target.files[0];
    if(!this.file){//when user attach a file and then again open attachment window but this time clicked on cancel. In this case it is undefined
      return;
    }
    var fileName = this.file.name;
    fileName = fileName.substr(0, fileName.lastIndexOf('.'));// || fileName ;//skip extension, parallel operator will handle extension less file
    this.myFormGroup.controls.name.setValue(fileName);
    this.docNameRef.nativeElement.focus();
  }

  save() {
    this.saving = true;
    this.bpmnService.addMetaDocument(this.myFormGroup.value).subscribe(
      data => {
        this.saving = false;
        this.documents.unshift(this.myFormGroup.value);
        this.myFormGroup.reset();
        this.file = undefined;
        this.formRef.nativeElement.reset();//clear the file input so that change event will trigger with same file selection as well
        this.toastService.success('Data saved successfully.')
      },
      error => {
        this.saving = false
        this.toastService.error('An error occurred while saving the data. Please try again or contact administrator.')
      }
    );
  }

  delete(item) {

    var items = this.documents;
    if (!item.id) {//local item
      return items.splice(items.indexOf(item), 1);
    }
    item.deleting = true;
    this.bpmnService.deleteMetaDocument(item).subscribe(
      data => {
        items.splice(items.indexOf(item), 1);
        this.toastService.success('Item deleted successfully.')
      },
      error => {
        item.deleting = false
        this.toastService.error('An error occurred while deleting the data. Please try again or contact administrator.')
      }
    );

  }

}
