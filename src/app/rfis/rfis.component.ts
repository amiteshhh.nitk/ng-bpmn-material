import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rfis',
  templateUrl: './rfis.component.html',
  styleUrls: ['./rfis.component.scss']
})
export class RfisComponent implements OnInit {
  isPageLoaded;
  constructor() { }

  ngOnInit() {
    var self = this;
    setTimeout(() => self.isPageLoaded = true, 1000);
  }

}
