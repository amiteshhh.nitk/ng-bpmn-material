import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material';


export var MatSelectionModelHelper = {

      /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected:(selectionModel : SelectionModel<any>, matDataSource : MatTableDataSource<any>) => {//: SelectionModel<any>
    const numSelected = selectionModel.selected.length;
    const numRows = matDataSource.data.length;
    return numSelected === numRows;
  },

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle:(selectionModel: SelectionModel<any>, matDataSource : MatTableDataSource<any>)=> {
    MatSelectionModelHelper.isAllSelected(selectionModel, matDataSource) ?
      selectionModel.clear() :
      matDataSource.data.forEach(row => selectionModel.select(row));
  }
}
