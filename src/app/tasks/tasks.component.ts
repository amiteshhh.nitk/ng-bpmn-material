import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  isPageLoaded;
  constructor() { }

  ngOnInit() {
    var self = this;
    setTimeout(() => self.isPageLoaded = true, 1000);
  }

}
