import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { LayoutModule} from '@angular/cdk/layout';

//third party encapsulated custom libs module
import { AppMaterialModule } from './libs/app-material.module';
//end of third party libs

//interceptor
import { CorsInterceptor } from './common/interceptor/cors.interceptor';
//

//Authguard
import { AuthGuardService } from './common/services/authguard.service';
//

//services
import { ApiServicesService } from './common/services/api-services.service';
import { AuthenticationService } from './common/services/authentication.service';
import { LoginService } from './login/login.service';
import { MessageService } from './common/services/message.service';
import { DialogService } from './common/dialog/dialog.service';
import { DashboardService } from './dashboard/dashboard.service';
import { ProjectsService } from './projects/projects.service';
import { BpmnService } from './bpmn/bpmn.service';
//

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { SpinnerComponent } from './common/spinner/spinner.component';
import { DialogComponent } from './common/dialog/dialog.component'
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';
import { GuestComponent } from './layout/guest/guest.component';
import { DashboardLayoutComponent } from './layout/dashboard-layout/dashboard-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DocumentsComponent } from './documents/documents.component'
import { RfisComponent } from './rfis/rfis.component'
import { ProjectsComponent } from './projects/projects.component'
import { ProjectEditComponent } from './project-edit/project-edit.component'
import { TasksComponent } from './tasks/tasks.component'
import { HelpComponent } from './help/help.component';
import { PhasesComponent } from './phases/phases.component';
import { BpmnComponent } from './bpmn/bpmn.component';
import { FilterComponent } from './common/filter/filter.component';
import { FilterDirective } from './common/filter/filter.directive';
import { FilterService } from './common/filter/filter.service';
import { ToastComponent } from './common/toast/toast.component';
import { ToastService } from './common/toast/toast.service';
import { BpmnDiagramComponent } from './bpmn/diagram/bpmn-diagram.component';
import { BpmnTaskComponent } from './bpmn/task/bpmn-task.component';
import { BpmnVariableComponent } from './bpmn/variable/bpmn-variable.component';
import { StatusIndicatorComponent } from './common/status-indicator/status-indicator.component';
import { StatusColorService } from './common/status-indicator/status-color.service';
import { BpmnDocumentComponent } from './bpmn/document/bpmn-document.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    LoginComponent,
    GuestComponent,
    DashboardLayoutComponent,
    SpinnerComponent,
    DashboardComponent,
    DocumentsComponent,
    RfisComponent,
    ProjectsComponent,
    ProjectEditComponent,
    TasksComponent,
    HelpComponent,
    PhasesComponent,
    BpmnComponent,
    DialogComponent,
    FilterComponent,
    FilterDirective,
    ToastComponent,
    BpmnDiagramComponent,
    BpmnTaskComponent,
    BpmnVariableComponent,
    StatusIndicatorComponent,
    BpmnDocumentComponent
  ],
  entryComponents:[DialogComponent, ToastComponent, BpmnVariableComponent, BpmnTaskComponent],
  imports: [
    //core
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    // LayoutModule,
    //libs
    AppMaterialModule,
    //app
    AppRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CorsInterceptor,
      multi: true
    },
    FormBuilder,
    ApiServicesService,
    AuthGuardService,
    AuthenticationService,
    LoginService,
    MessageService,
    DialogService,
    DashboardService,
    FilterService,
    ProjectsService,
    BpmnService,
    ToastService,
    StatusColorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
