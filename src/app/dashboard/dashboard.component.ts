import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { DashboardService } from './dashboard.service';
import { MessageService } from '../common/services/message.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  status: string;
  due_date: string;
  description: string;
  status_date: string;
  staticData = [
    { 
    "url" : "", 
    "description" : "Project Multistory at 3300 Pickwick awaiting completion", 
    "status_date" : "2018-07-31",
    "status" : "normal", 
    "due_date" : "None" 
    }, {
    "url" : "", 
    "description" : "Task Floor slab casting' awaiting completion", 
    "status_date" : "2018-08-01",
    "status" : "urgent", 
    "due_date" : "2018-09-15" 
  }];
  dataSource: any;
  displayedColumns: string[] = ['status', 'due_date', 'description', 'status_date'];
  isPageLoaded;
  subscription;
  navigationSubscription;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private dashboardService: DashboardService, private messageService: MessageService, private router: Router) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.isPageLoaded = false;
        this.dataSource = [];
        setTimeout(()=>{   
          this.dataSource = new MatTableDataSource(this.staticData);
          this.isPageLoaded = true;
        }, 3000);
      }
    });
   }

  ngOnInit() {
    //self.getDashboardData();
    this.isPageLoaded = true;
    this.dataSource = new MatTableDataSource(this.staticData);
    this.dataSource.sort = this.sort;
    this.subscription = this.messageService.on('app-filter-input').subscribe(item => this.applyFilter(item.data));
    var firstname = localStorage.getItem('first-name');
    // history.pushState(null, null, location.href);
    // window.onpopstate = function(event) {
    //   history.go(1);
    // };
  }

  private getDashboardData() {
    this.dashboardService.getDashboardEntries()
      .subscribe(data => {
        console.log(data) ;
        this.isPageLoaded = true;
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort; 
      },
      error => {
        console.log(error) ;
        this.isPageLoaded = true;
      });
  }

  private applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue;
  }

  private navigate(url: string){
    this.router.navigate(['/projects']);
  }

}
