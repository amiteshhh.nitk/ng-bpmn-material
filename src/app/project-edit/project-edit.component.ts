import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import 'rxjs/Rx';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProjectEditComponent implements OnInit {

  loading = false;
  userForm: any;
  closeResult: string;
  errorMessage: any;
  id:string;
  projectName:string;
  name: string;
  isSaving:boolean;

  constructor(private router: Router, public dialog: MatDialog, public dialogRef: MatDialogRef<ProjectEditComponent>, @Inject(MAT_DIALOG_DATA) data) { 
    this.projectName = data.name;
    this.id = data._id;
  }

  ngOnInit() {

  }

  projectDetails = { "description" : "", "role_names" : ["QualityAssurance", "Safety-Assurance", "SiteManagement"], "assigned_roles" : [] }
  assigneeDetails = [{ "person_id" : "56f124dfd5d8ad25b1325b3f","name" : "Tester Tester" }, { "person_id" :"5acf50df4ac8efe4e19e91b3", "name" : "Tester3 Tester" }]
  assignedRoles = [{ "role_name" : "QualityAssurance", "person_id" : "56f124dfd5d8ad25b1325b3f", "person_name" : "Tester Tester" }, { "role_name" : "Safety-Assurance", "person_id" : "5acf50df4ac8efe4e19e91b3", "person_name" : "Tester3 Tester" }]
  
  roleControl = new FormControl('', [Validators.required]);
  roleNames = this.projectDetails.role_names;

  assigneeControl = new FormControl('', [Validators.required]);
  assignees = this.assigneeDetails;

  save() {
    this.isSaving = true;
    //this.dialogRef.close();
  }

  close() {
    this.isSaving = false;
    this.dialogRef.close();
  }

  addAssignedRoles(role, assignee){
    if(role && assignee.person_id && assignee.name){
      this.assignedRoles.push({ "role_name" : role, "person_id" : assignee.person_id, "person_name" : assignee.name })
    }
  }

  deleteAssignedRoles(idx){
    this.assignedRoles.splice(idx, 1);

  }

}
