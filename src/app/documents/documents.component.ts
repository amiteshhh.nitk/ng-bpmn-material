import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {
  isPageLoaded;
  constructor() { }

  ngOnInit() {
    var self = this;
    setTimeout(() => self.isPageLoaded = true, 1000);
  }

}
