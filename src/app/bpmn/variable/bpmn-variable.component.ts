import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { ToastService } from '../../common/toast/toast.service';
import { BpmnService } from './../bpmn.service';

@Component({
  selector: 'app-bpmn-variable',
  templateUrl: './bpmn-variable.component.html',
  styleUrls: ['./bpmn-variable.component.scss']
})
export class BpmnVariableComponent implements OnInit {
  dataSource;canEdit;
  saving = false;
  savingdateTime = false;
  displayedColumns = ['name', 'type', 'value'];
  form;
 

  constructor(private toastService: ToastService, private bpmnService: BpmnService, @Inject(MAT_DIALOG_DATA) public allData: any, public dialogRef: MatDialogRef<BpmnVariableComponent>) { 
    this.dataSource = this.allData.variables;
    this.canEdit = this.allData.canEdit;
  }

  ngOnInit() {
    ///TODO formgroup time extraction from allData.start_datetime and assign to startTime
    // var dateTime = this.allData.start_datetime//should be iso date time string or millisecond value. e.g JSON.stringify(new Date())
    var dateTime = new Date("2018-11-12T08:27:13.186Z");
    var datePart = new Date(dateTime);//no need to set time part to 0 as it wont make any difference
    var timePart = dateTime.getHours() + ':' + dateTime.getMinutes();//no need to set time part to 0 as it wont make any difference
    this.form = new FormGroup({
      startDate: new FormControl(datePart, [Validators.required]),
      startTime: new FormControl(timePart, [Validators.required])
    })
  }

  save() {
    this.saving = true;
    this.bpmnService.saveVariables(this.dataSource).subscribe(
      data => {
        this.toastService.success('Data saved successfully.')
        this.dialogRef.close();
      },
      error => {
        this.saving = false
        this.toastService.error('An error occurred while saving the data. Please try again or contact administrator.')
      }
    );
  }
  saveDateTime() {
    this.savingdateTime = true;
    var hh_mm = (this.form.value.startTime || '00:00').split(':');
    var hh = Number(hh_mm[0]), mm = Number(hh_mm[1]);
    var d = this.form.value.startDate;
    d.setHours(hh, mm);
    console.log('saving start date with time', d);
    this.bpmnService.saveVariableDateTime(d).subscribe(
      data => {
        this.toastService.success('Data saved successfully.')
        this.dialogRef.close();
      },
      error => {
        this.savingdateTime = false
        this.toastService.error('An error occurred while saving the data. Please try again or contact administrator.')
      }
    );
  }

}
