import { Component, OnInit, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { MatDialog, MatTableDataSource } from '@angular/material'
import Viewer from 'bpmn-js/lib/Viewer';

import { BpmnService } from '../bpmn.service'
import { ToastService } from './../../common/toast/toast.service';
import { StatusColorService } from './../../common/status-indicator/status-color.service';

import { BpmnVariableComponent } from '../variable/bpmn-variable.component'
import { BpmnTaskComponent } from './../task/bpmn-task.component';

var self//this is needed as `this` context will change in non-angular context like bpmn events callback
@Component({
  selector: 'app-bpmn-diagram',
  templateUrl: './bpmn-diagram.component.html',
  styleUrls: ['./bpmn-diagram.component.scss']
})
export class BpmnDiagramComponent implements OnInit {
  params; saving = false; selectedBpmn; dialogRef; selectedItemType; subTasks: Array<any>;
  //elementType will decide what to show among following: property list popup, task hover details, task list popup, timer duration popup, sub process bpmn load
  private allData: any;//service response
  private bpmnElements: Array<any>;
  private bpmnViewer: any;
  private bpmnOverlays: any;
  @ViewChild('durationTemplateRef') durationTemplateRef: TemplateRef<any>;
  @ViewChild('taskAndDocumentTemplateRef') taskAndDocumentTemplateRef: TemplateRef<any>;
  @ViewChild('taskHoverElRef') taskHoverElRef: ElementRef;



  bpmnData = { xml: null, timers: null, variables: null, activities: null, calls: null, admin_person_id: null, phase_status: null }

  constructor(activatedRoute: ActivatedRoute, private bpmnService: BpmnService, private dialog: MatDialog, private toastService: ToastService, private statusColorService: StatusColorService) {
    self = this;
    this.params = activatedRoute.snapshot.params;
  }

  ngOnInit() {
    this.bpmnService.getBpmnDetails(this.params.bpmnId).subscribe(
      data => {
        this.allData = data;
        this.bpmnViewer = new Viewer({
          container: "#bpmnDiagram"
        });
        this.renderBpmnDiagram(data);
        this.setAuth();

      }, // success path
      error => console.log(error) // error path
    );
  }

  private setAuth() {
    var canEdit = this.allData.phase_status === 'defined' ///TODO cond && this.phase.is_managed
    this.allData.canEdit = canEdit;
  }

  private renderBpmnDiagram(data) {
    this.bpmnViewer.importXML(data.xml, function (err) {
      if (err) {
        self.toastService.error('An error occured while rendering BPMN diagram.')
        console.log("error rendering", err);
      } else {
        self.onBpmnRender(data);//called in this way to hook back `this` to this class component
      }
    });
  }

  private onBpmnRender(data) {
    var canvas = this.bpmnViewer.get('canvas');
    canvas.zoom('fit-viewport');

    this.bpmnOverlays = this.bpmnViewer.get('overlays');

    this.annotateBPMN(this.allData.timers, 36);
    this.annotateBPMN(this.allData.activities, 100);
    this.annotateBPMN(this.allData.calls, 100);

    this.bpmnElements = this.allData.timers.concat(this.allData.activities, this.allData.calls);
    this.hookEvent();
  }

  private annotationOverlayHtml(bpmnElement) {
    return '<div style="background-color:' + this.statusColorService.getColor(bpmnElement.status, 'phase') + '; white-space:nowrap; padding:3px; border:1px solid #333; border-radius:5px; text-align:center; font-size: x-small; width:50px;">' + bpmnElement.duration + '</div>';
  }

  private annotateBPMN(bpmnElements, width) {
    bpmnElements.forEach(bpmnElement => {
      this.bpmnOverlays.add(bpmnElement.bpmn_id, {
        position: {
          top: -22,
          left: (width - 60) / 2
        },
        html: this.annotationOverlayHtml(bpmnElement)
      });
    });
  }

  private hookEvent() {
    // var events = [
    //   //'element.click',
    //   // 'element.dblclick',
    //   'element.hover'
    // ];
    // this.processActivities.forEach(item =>//target event attachment
    //   document.querySelector('[data-element-id=' + item.bpmn_id + ']').addEventListener('click', () => alert(1))
    // );

    var eventBus = this.bpmnViewer.get('eventBus');
    eventBus.on('element.hover', event => self.onNodeHover(event));
    // events.forEach((event) => {
    //   eventBus.on(event, event => self.eventHandler(event));
    // });
    var bpmnElements = this.bpmnViewer.get('elementRegistry');
    bpmnElements.forEach((element) => {
      this.attachClickEventListner(element);
    });
  }


  private attachClickEventListner(element) {
    var selectedBpmn = this.findBpmnElement(element);
    var domElement = document.querySelector('[data-element-id=' + element.id + ']');
    if (selectedBpmn && selectedBpmn.elementType === 'timer') {//show timer duration edit popup
      domElement.addEventListener('click', (event) => {
        this.onEventFire(event, selectedBpmn);
        this.showTimerDurationDialog()
      })
    } else if (selectedBpmn && selectedBpmn.elementType === 'activity') {//show Task edit popup
      domElement.addEventListener('click', (event) => {
        this.onEventFire(event, selectedBpmn);
        this.showTaskDialog()
      })
    } else if (this.selectedBpmn && this.selectedBpmn.elementType === 'subprocessCall' && element.type == 'bpmn:CallActivity') {
      //(double-click to display called process) 
      domElement.addEventListener('dblclick', (event) => {
        this.onEventFire(event, selectedBpmn);
        this.loadSubprocess();
      })
    } else if (element.type === 'bpmn:Process') {//show process variable edit popup
      // this.showProcessVariableDialog();
      domElement.addEventListener('click', (event) => {
        this.onEventFire(event, selectedBpmn);
        this.showProcessVariableDialog()
      })
    }
  }

  private onEventFire(event, selectedBpmn) {
    this.removeBpmnHoverInfo();
    this.selectedBpmn = selectedBpmn;
    event.stopPropagation();
  }

  /*   private eventHandler(event) {
      var element = event.element;
      // this.activeBpmnNode = element;
      console.log(event.type, element)
      this.selectedItemType = null;
      event.stopPropagation();//in bpmn 2.4 its firing twice
      switch (event.type) {
        case "element.hover":
          this.onNodeHover(element);
          break;
        case "element.click":
          this.onNodeClick(element);
          break;
        case "element.dblclick":
          this.doubleClickElement(element);
          break;
      }
    } */

  private findBpmnElement(element) {
    return this.bpmnElements.find(item => item.bpmn_id === element.id);
  }

  /* ********** START HOVER IMPLEMENTATION ***************  */
  // hoverOverlayId = null;
  hoverOverlayIds = [];//sometime two instance of popover getting opened on single hover event especially in IE edge which leads to permanent task popover in the screen, therefore we are maintaining array
  private removeBpmnHoverInfo() {
    // if (this.hoverOverlayId != null) {
    //   this.bpmnOverlays.remove(this.hoverOverlayId);
    //   this.selectedBpmn = this.hoverOverlayId = null;
    // }
    this.hoverOverlayIds.forEach((hoverOverlayId) => this.bpmnOverlays.remove(hoverOverlayId));
    this.hoverOverlayIds.length = 0;
    this.selectedBpmn = null;
  }
  
  private onNodeHover(event) {
    var element = event.element;
    this.removeBpmnHoverInfo();

    /* if (element.type === 'bpmn:Process') {//commented out as we can deduce the condition by date alone
      return;
    } */
    this.selectedBpmn = this.findBpmnElement(element);
    if (!this.selectedBpmn || !this.selectedBpmn.start) {
      return;
    }
    if (this.selectedBpmn.tasks) {
      this.subTasks = this.selectedBpmn.tasks.filter((item) => item.type !== 'main');
    } else {
      this.subTasks = null;
    }
    this.selectedItemType = 'hover:task'
    setTimeout(() => {//lets wait for markup to be rendered bkz of *ngIf
      var htmlStr = this.taskHoverElRef.nativeElement.innerHTML;

      var hoverOverlayId = this.bpmnOverlays.add(element.id, {
        position: {
          top: -5,//minus value to hide the border of bpmn node
          right: -5
        },
        html: htmlStr
      });
      this.hoverOverlayIds.push(hoverOverlayId);//workaround to fix intermittent persistent overlay
    });
  }
  /* ********** END HOVER IMPLEMENTATION ***************  */
  /* ********** DOUBLE CLICK IMPLEMENTATION ***************  */
  private loadSubprocess() {
    this.removeBpmnHoverInfo();

    // this.selectedBpmn = this.findBpmnElement(element);
    // if (!this.selectedBpmn || !this.selectedBpmn.name || this.selectedBpmn.elementType !== 'subprocessCall') {
    //   return;
    // }
    alert('Oops!!! Redirection not implemented yet.')
    // var dtIdx = $window.location.href.indexOf('bpmn?');
    // var href = $window.location.href.substring(0, dtIdx + 5);
    // var newHref = href + 'project_id=' + self.projectId + '&phase_id=' + self.phaseId + '&process=' + processName;
    // $window.location.href = newHref;
  }
  /* ********** END OF DOUBLE CLICK IMPLEMENTATION ***************  */
  /* ********** CLICK IMPLEMENTATION ***************  */

  private onNodeClick(element) {

    this.removeBpmnHoverInfo();

    this.selectedBpmn = this.findBpmnElement(element);
    // console.log('clicked', element.type, this.selectedBpmn.elementType, element)
    if (this.selectedBpmn && this.selectedBpmn.elementType === 'timer') {//show timer duration edit popup
      this.showTimerDurationDialog();
    } else if (this.selectedBpmn && this.selectedBpmn.elementType === 'activity') {//show Task edit popup
      this.showTaskDialog();
    } /* else if (this.selectedBpmn && this.selectedBpmn.elementType === 'subprocessCall' && element.type == 'bpmn:CallActivity') {
        //(double-click to display called process) 
      } */
    else if (element.type === 'bpmn:Process') {//show process variable edit popup
      this.showProcessVariableDialog();
    }
  }
  private showProcessVariableDialog() {
    this.selectedItemType = 'click:variable'
    this.dialog.open(BpmnVariableComponent, {
      panelClass: ['edit-form-dialog', 'has-dialog-close-icon', 'lightBlue-bg-theme', 'dialog-md', 'dialog-container-padding-top-0'],
      // width: '600px',
      autoFocus: false,
      data: this.allData
    });
  }
  private showTaskDialog() {
    this.selectedItemType = 'click:task'
    this.dialog.open(BpmnTaskComponent, {
      // this.dialogRef = this.dialog.open(this.taskAndDocumentTemplateRef, {
      panelClass: ['edit-form-dialog', 'has-dialog-close-icon', 'lightBlue-bg-theme', 'dialog-lg', 'dialog-container-padding-top-0'],
      width: '1100px',
      position: { top: '100px' },
      data: {
        selectedBpmn: this.selectedBpmn,
        allData: this.allData
      },
      autoFocus: false
    });
  }

  private showTimerDurationDialog() {
    this.selectedItemType = 'click:timer'
    this.dialogRef = this.dialog.open(this.durationTemplateRef, {
      panelClass: 'lightBlue-bg-theme'
    });
  }
  saveDuration() {
    this.saving = true;
    this.bpmnService.saveDuration({}).subscribe(
      data => {
        this.saving = false;//todo
        this.toastService.success('Data saved successfully.')
        this.dialogRef.close();
      },
      error => {
        this.saving = false
        this.toastService.error('An error occurred while saving the data. Please try again or contact administrator.')
      }
    )
  }
}
