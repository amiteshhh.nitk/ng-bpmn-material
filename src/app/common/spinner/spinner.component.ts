import { Component, OnInit, Input } from '@angular/core';
/** Usage
 *  <app-spinner class="inline-spinner"></app-spinner>
 *  <app-spinner class="center-spinner"></app-spinner>
 *  <app-spinner class="center-line-spinner"></app-spinner>
 *  <app-spinner diameter="48"></app-spinner>
 *  <app-spinner button class="inline-spinner"></app-spinner><!-- button attribute added to set diameter to 24 -->
 */

@Component({
  selector: 'app-spinner',
  template: '<mat-spinner [diameter]="diameter">{{title}}</mat-spinner>',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  @Input() button;
  @Input() diameter = 36;
  constructor() { }
  ngOnInit() {
    if (this.button === '') {
      this.diameter = 24;
    } 
  }
}
