
import { Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

/**
 * This service acts as a helper to support column level and global filter support
 * It also provides some utility function like initCap and unique.
 */
@Injectable()
export class FilterService {

  constructor() { }
  /** Recommended way of associating filter configuration to MatTableDataSource instance.  */
  createFilter(dataSource: MatTableDataSource<any>, columns: Array<string>, filterParams?: IFilterParams): void {
    dataSource.filterPredicate = this.filterPredicate;
    dataSource['filterSetting'] = this.createFilterSetting(columns, filterParams, dataSource.data)
  }
  /** Creates filter setting object. Use this method to explicitely associate filterSetting to MatTableDataSource instance.
   * If data is passed it will build up the dropdown filter values as well. 
   */
  createFilterSetting(columns: Array<string>, filterParams?: IFilterParams, data?: Array<any>): IFilterSetting {
    var columnDefs = {};
    columns.forEach((column) => columnDefs[column] = '')
    var filterSetting = { filterVals: { columns: columnDefs, quickFilterText: '' }, filterParams: filterParams };
    if (filterParams && data && data.length) {
      this.refreshDropdownFilterValues(filterSetting, data);
    }
    return filterSetting;
  }
  /** Clear the filter input for each columns along with global filter input. It doesn't trigger filtering. */
  clearFilterState(filterSetting: IFilterSetting) {
    filterSetting.filterVals.quickFilterText = ''
    for (let column in filterSetting.filterVals.columns) {
      filterSetting.filterVals.columns[column] = ''
    }
  }
  /** Use this method to rebuild the values for dropdown filter. Required only when  `buildValuesFromData` is set to true.  */
  refreshDropdownFilterValues(filterSetting: IFilterSetting, data: Array<any>) {
    if (!data || !data.length) {
      return;
    }
    for (let column in filterSetting.filterParams) {
      let filterParam = filterSetting.filterParams[column]
      if (filterParam.buildValuesFromData) {
        filterParam.values = this.unique(data.map((item) => item[column])).sort();
      }
    }
  }
  /** Returns a new array containing unique elements */
  unique(arr: Array<any>) {
    return arr.filter((x, i, a) => a.indexOf(x) === i);
  }
  /** Capitalize first letter of each word. Rest of letters converted to small. `_` is replaced with space. */
  initCap(input: string) {//https://stackoverflow.com/questions/19863402/convert-a-sentence-to-initcap-camel-case-proper-case
    if (typeof input !== 'string') {
      return input;
    }
    var delimeter = '_';// replace it with regex if need to accomodate more use case
    input = input.replace(delimeter, ' ');

    //Generic one to cover the sentance as well 
    return input.toLowerCase().replace(/(?:^|\s)[a-z]/g, function (m) {
      return m.toUpperCase();
    });
  }
  /** Trigger the filter by modifying  MatTableDataSource `filter` property. */
  applyFilter(dataSource: MatTableDataSource<any>, quickFilterText?) {
    var filterSetting = dataSource['filterSetting'];
    if (quickFilterText !== undefined) {
      filterSetting.filterVals.quickFilterText = quickFilterText;
    }
    dataSource.filter = JSON.stringify(filterSetting.filterVals);
  }
  /** Predicate function representing filter logic */
  filterPredicate(data: any, filtersJson: string): boolean {
    //`this` refres to dataSource:MatTableDataSource
    var columnFilterMatch = true;
    //var filterVals = JSON.parse(filtersJson);

    var filterSetting = this['filterSetting'];
    var filterColumns = filterSetting.filterVals.columns;
    var quickFilterTextLcased = (filterSetting.filterVals.quickFilterText || '').toString().toLowerCase();
    var quickFilterMatch = !quickFilterTextLcased;

    for (let column in filterColumns) {//assuming string data for now
      var filterColumnVal = filterColumns[column];
      var columnValLcased = (data[column] || '').toString().toLowerCase();
      if (filterColumnVal) {
        ///TODO: based on data type comparisons
        if (Array.isArray(filterColumnVal)) {//dropdown type filter is performed case sensitive complete match
          if (filterColumnVal.length && !filterColumnVal.includes(data[column])) {
            columnFilterMatch = false;
            break;
          }
        } else if (!columnValLcased.includes(filterColumnVal.toLowerCase())) {
          columnFilterMatch = false;
          break;
        }
      } else if (!quickFilterMatch && quickFilterTextLcased) {
        if (columnValLcased.includes(quickFilterTextLcased)) {
          quickFilterMatch = true;
        }
      }
    }
    return columnFilterMatch && quickFilterMatch;
  }

}


/** Supported filter column types */
export type IFilterType = 'number' | 'text' | 'date' | 'dropdown';

/** Meta data for filter column. Its mandatory for dropdown filters. However later can be utilized for other filter types like date etc. */
export interface IFilterParam {
  /** Array of items to be shown in dropdown filter dialog. Item should not be a complex object. */
  values?: Array<any>,
  /** Used to format the value before displaying in dropdown. Useful to convert the data on fly for simple id kind of field. */
  valueFormatter?: Function,
  /** Pass it to true if you don't provide the values shown in dropdown filter. Service will extract the values from datasource. */
  buildValuesFromData?: boolean,
  /** Type of column to have more strict filter comparison. For now 'dropdown' is only supported value. Later we may treat number, text and date obj differently.
   * You may also pass the filterType directly in markup <app-filter filterType="dropdown"></app-filter>. It will be handy for date, number etc in near future.
  */
  filterType?: IFilterType;
}
/** Dictionary of items each representing <IFilterParam>  */
export interface IFilterParams {
  [key: string]: IFilterParam
}
/** Main filter object containing all information to assist in implementing filter */
export interface IFilterSetting {
  /** Filter models to hold user input*/
  filterVals: {
    /** Hold the value of global filter to be matched across all columns. */
    quickFilterText: string,
    /** Dictionary containg one property for each column to store the filter input. */
    columns: { [key: string]: any },
  },
  filterParams?: IFilterParams
}