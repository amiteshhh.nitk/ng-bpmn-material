import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiServicesService } from '../services/api-services.service';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthenticationService {

    constructor(private http: HttpClient, private _apiServicesService: ApiServicesService) { }

    /** 
      * Function to remove user from local storage and also from Api
      * Return response from Api
    **/
    public logout(): Observable<any> { 
        return this.http.post(this._apiServicesService.getApiBaseUrl() + 'etc/LogoutPost', {})
        .map(res => { 
            console.log("Logout User: "+ res);            
            return res;
        })
        .catch(this.handleErrorObservable);
    }

    /** 
      * Function to check if logged user having admin role or not
      * Return response from based on Api
    **/
    public isAdminRole(): boolean {
        let _currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (_currentUser != null && _currentUser != "") {
            var _dataAdmin = _currentUser.roles.indexOf("BW-Data-Admin");
            var _admin = _currentUser.roles.indexOf("BW-Admin");
            if (parseInt(_dataAdmin) >= 0 && parseInt(_admin)) {
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }

    /** 
      * Function to handle exception from Api response.
    **/
    private handleErrorObservable(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }
}


