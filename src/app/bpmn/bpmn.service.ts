import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

//below rxjs import to mock service
import { Observable } from 'rxjs/Observable';
// import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import {_throw} from 'rxjs/observable/throw';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';


@Injectable()
export class BpmnService {

  constructor(private http: HttpClient) { }

  getBpmnDetails(id) {
    var url = 'assets/bpmn-sample1.json';
    return this.http.get(url)
  }

  saveTask(param) {
    var url = 'assets/bpmn-sample1.json';
    return this.http.get(url)
  }
  deleteTask(param) {
    var url = 'assets/bpmn-sample1.json';
    return this.http.get(url)
  }

  saveVariables(param) {
    var url = 'assets/bpmn-sample1.json';
    return this.http.get(url)
  }

  saveVariableDateTime(param: any) {
    var url = 'assets/bpmn-sample1.json';
    return this.http.get(url)
  }

  saveDuration(param) {
    var url = 'assets/bpmn-sample1.json';
    return this.http.get(url)
  }

  addMetaDocument(param) {
    var url = 'assets/bpmn-sample1.json';
    return this.http.get(url)
  }
  deleteMetaDocument(param) {
    var url = 'assets/bpmn-sample1.json';
    return this.http.get(url)
  }
  getRolesByTaskType(type?: string) {
    var fakeResponse = ["Quality-Assurance", "Safety-Assurance", "Site-Management"];
    return Observable.of(fakeResponse).delay(1000);
  }
  getAssignees(type, role) {
    var fakeResponse = [{
      "_id": "5ba8e2ff63259012d2e2edcb",
      "name": "Sourav Sahoo"
    }, {
      "_id": "d",
      "name": "Sanjay Dasgupta"
    }];
    // return type === 'review' ? Observable.throw(new Error('oops!')).delay(3000)//delay not working
    // return type === 'review' ? ErrorObservable.create('oops').delay(3000)//delay not working
    return type === 'review' ? _throw('oops').delay(3000)//delay not working
      : Observable.of(fakeResponse).delay(1000);
  }
}
